﻿using System.ServiceModel;
using Test.SmsIT.Service.Data;

namespace Test.SmsIT.Service.Contracts
{
    [ServiceContract]
    public interface ITransferService
    {
        [OperationContract]
        BookCategoryDictionary GetBookCategoryList();
    }
}