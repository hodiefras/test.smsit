﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Windows;
using Autofac;
using Caliburn.Micro;
using Test.SmsIT.Gateway;
using Test.SmsIT.Model;
using Test.SmsIT.Service.ClientModel;
using Test.SmsIT.Service.Contracts;
using Test.SmsIT.View;
using Test.SmsIT.ViewModel;
using IContainer = Autofac.IContainer;

namespace Test.SmsIT.App.Bootstrapper
{
    internal sealed class AppAutofacBootstrapper : BootstrapperBase
    {
        #region Consturctor and public properties

        public AppAutofacBootstrapper()
        {
            Initialize();
            AppDomain.CurrentDomain.UnhandledException += (sender, e) =>
            {
                // TODO Logging
            };
        }

        /// <summary>
        ///     IoC container
        ///     In this case it is autofac
        /// </summary>
        protected IContainer Container { get; set; }

        public bool EnforceNamespaceConvention { get; set; }

        public bool AutoSubscribeEventAggegatorHandlers { get; set; }

        public Type ViewModelBaseType { get; set; }

        public Func<IWindowManager> CreateWindowManager { get; set; }

        public Func<IEventAggregator> CreateEventAggregator { get; set; }

        #endregion

        #region Configure container private-logic methods

        protected override void OnStartup(object sender, StartupEventArgs e)
        {
            DisplayRootViewFor<ShellViewModel>();
        }

        protected override void Configure()
        {
            ConfigureBootstrapper();
            var config = new TypeMappingConfiguration
            {
                DefaultSubNamespaceForViews = "Test.SmsIT.View",
                DefaultSubNamespaceForViewModels = "Test.SmsIT.ViewModel"
            };
            ViewLocator.ConfigureTypeMappings(config);
            ViewModelLocator.ConfigureTypeMappings(config);

            if (CreateWindowManager == null)
                throw new ArgumentNullException("CreateWindowManager");

            if (CreateEventAggregator == null)
                throw new ArgumentNullException("CreateEventAggregator");

            var builder = new ContainerBuilder();
            ConfigureContainer(builder);

            Container = builder.Build();
        }


        private void ConfigureBootstrapper()
        {
            EnforceNamespaceConvention = true;
            AutoSubscribeEventAggegatorHandlers = false;
            ViewModelBaseType = typeof (INotifyPropertyChanged);
            CreateWindowManager = () => (IWindowManager) new WindowManager();
            CreateEventAggregator = () => (IEventAggregator) new EventAggregator();
        }

        protected override IEnumerable<Assembly> SelectAssemblies()
        {
            return new[]
            {
                GetType().Assembly,
                typeof (ShellViewModel).Assembly,
                typeof (ShellView).Assembly
            };
        }


        private void ConfigureContainer(ContainerBuilder builder)
        {
            //  register view models
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray())
                //  must be a type that ends with ViewModel
                .Where(type => type.Name.EndsWith("ViewModel"))
                //  must implement INotifyPropertyChanged (deriving from PropertyChangedBase will statisfy this)
                .Where(type => type.GetInterface(typeof (INotifyPropertyChanged).Name) != null)
                //  registered as self
                .AsSelf()
                //  always create a new one
                .InstancePerDependency();

            //  register views
            builder.RegisterAssemblyTypes(AssemblySource.Instance.ToArray()) //  must be a type that ends with View
                .Where(type => type.Name.EndsWith("View"))
                //  registered as self
                .AsSelf()
                //  always create a new one
                .InstancePerDependency();

            //  register the single window manager for this container
            builder.Register(c => CreateWindowManager()).InstancePerLifetimeScope();
            //  register the single event aggregator for this container
            builder.Register(c => CreateEventAggregator()).InstancePerLifetimeScope();
            //  register the single adapter for container
            //builder.Register(c => CreateServiceAdapter()).InstancePerLifetimeScope();

            builder.RegisterType<ShellViewModel>()
                .As<IShell>()
                .WithParameter("windowsManager", CreateWindowManager)
                .WithParameter("events", CreateEventAggregator);

            builder.RegisterType<ShellViewModel>()
                .As<IShell>();

            builder.RegisterType<Communication<ITransferService>>()
                .WithParameter("endpointName", "Test.SmsIT.Service.Contracts.ITransferService");

            builder.RegisterType<BookCategoryCollection>().UsingConstructor(typeof (ITransferServiceAdapter));

            builder.RegisterType<BookCategoryCollectionViewModel>().UsingConstructor(typeof (BookCategoryCollection));

            builder.RegisterType<TransferServiceAdapter>().As<ITransferServiceAdapter>();

            if (AutoSubscribeEventAggegatorHandlers)
                builder.RegisterModule<EventAggregationAutoSubscriptionModule>();
        }


        protected override object GetInstance(Type service, string key)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                object obj;
                if (Container.TryResolve(service, out obj))
                    return obj;
            }
            else
            {
                object obj;
                if (Container.TryResolveNamed(key, service, out obj))
                    return obj;
            }
            throw new Exception(string.Format("Could not locate any instances of contract {0}.", key ?? service.Name));
        }

        protected override IEnumerable<object> GetAllInstances(Type service)
        {
            return Container.Resolve(typeof (IEnumerable<>).MakeGenericType(new[] {service})) as IEnumerable<object>;
        }

        protected override void BuildUp(object instance)
        {
            Container.InjectProperties(instance);
        }

        #endregion
    }
}