﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Core;
using Caliburn.Micro;

namespace Test.SmsIT.App.Bootstrapper
{
    class EventAggregationAutoSubscriptionModule : Module
    {
        protected override void AttachToComponentRegistration(IComponentRegistry registry, IComponentRegistration registration)
        {
            registration.Activated += OnComponentActivated;
        }

        private static void OnComponentActivated(object sender, ActivatedEventArgs<object> e)
        {
            if (e == null)
                return;
            var handle = e.Instance as IHandle;
            if (handle == null)
                return;
            e.Context.Resolve<IEventAggregator>().Subscribe(handle);
        }
    }
}
