﻿namespace Test.SmsIT.Model
{
    /// <summary>
    ///     Model class for book
    /// </summary>
    public class Book
    {
        public Book(string name, string author, string description, string hyperLink)
        {
            Name = name;
            Author = author;
            Description = description;
            HyperLink = hyperLink;
        }

        public string Name { get; private set; }
        public string Author { get; private set; }
        public string Description { get; private set; }

        /// <summary>
        ///     Link to our book
        /// </summary>
        public string HyperLink { get; private set; }

        /// <summary>
        ///     Pathfile for images
        /// </summary>
        public string ImageSource { get; set; }
    }
}