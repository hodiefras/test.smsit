﻿using System.Collections.Generic;

namespace Test.SmsIT.Model
{
    public interface ITransferServiceAdapter
    {
        Dictionary<string, List<Book>> GetTransferBook();
    }
}