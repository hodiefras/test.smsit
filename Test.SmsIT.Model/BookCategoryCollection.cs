﻿using System.Collections.Generic;

namespace Test.SmsIT.Model
{
    /// <summary>
    /// Main model class
    /// Statefull with adapter for get info
    /// </summary>
    public class BookCategoryCollection
    {
        private readonly ITransferServiceAdapter transferServiceAdapter;

        public BookCategoryCollection(ITransferServiceAdapter initTransferServiceAdapter)
        {
            transferServiceAdapter = initTransferServiceAdapter;
        }

        public Dictionary<string, List<Book>> GetBookDictionary()
        {
            return transferServiceAdapter.GetTransferBook();
        }
    }
}