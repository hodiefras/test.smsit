﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Test.SmsIT.DAL.Data;

namespace Test.SmsIT.DAL.Data
{
    public interface IRepository
    {
        List<Book> GetBooksDictionary();
    }
}
