﻿namespace Test.SmsIT.DAL.Data
{
    /// <summary>
    ///     Class for represent logic of data-layer
    /// </summary>
    public class Book
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Link { get; set; }
    }
}