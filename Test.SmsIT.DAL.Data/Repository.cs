﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml;

namespace Test.SmsIT.DAL.Data
{
    /// <summary>
    ///     Class-repository
    ///     Need to make something like singleton
    /// </summary>
    public class Repository : IRepository
    {
        #region Private fields

        /// <summary>
        ///     Root path in xml-file
        /// </summary>
        private const string UserPath = "/root/Books/Book";


        //private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        /// <summary>
        ///     Logger
        /// </summary>
        /// <summary>
        ///     File path
        /// </summary>
        private readonly string xmlFileStorage;

        #endregion

        #region Open methods

        /// <summary>
        ///     Constructor
        /// </summary>
        public Repository()
        {
            xmlFileStorage = ConfigurationManager.AppSettings["storagepath"];
        }

        /// <summary>
        ///     Get dictionary
        /// </summary>
        /// <returns></returns>
        public List<Book> GetBooksDictionary()
        {
            //Logger.Debug("Load xml File");
            XmlDocument document = LoadXml();

            // take correct path.
            if (document.DocumentElement == null)
            {
                //Logger.Debug("Not found xml node");
                return null;
            }

            var result = new List<Book>();
            XmlNodeList xmlBooks = document.DocumentElement.SelectNodes(UserPath);
            foreach (XmlNode node in xmlBooks)
            {
                var book = new Book();
                XmlAttributeCollection attr = node.Attributes;
                if (attr == null) continue;
                book.Name = attr["Name"].Value;
                book.Author = attr["Author"].Value;
                book.Description = attr["Description"].Value;
                book.Category = attr["Category"].Value;
                book.Link = attr["Hyperlink"].Value;
                result.Add(book);
            }
            return result;
        }

        #endregion

        #region Helper methods

        /// <summary>
        ///     Loads XML document specified by the name.
        /// </summary>
        /// <returns></returns>
        private XmlDocument LoadXml()
        {
            var document = new XmlDocument();

            //Logger.Debug("Trying to open {0}", xmlFileStorage);
            // Verify pathfile.
            if (string.IsNullOrEmpty(xmlFileStorage) || string.IsNullOrEmpty(xmlFileStorage.Trim()))
            {
                //Logger.Warn("Provided file name is not valid (null or empty).");
                throw new ArgumentException("Provided file name is not valid (null or empty).", xmlFileStorage);
            }

            // Verify file for exist.
            if (!File.Exists(xmlFileStorage))
            {
                //Logger.Error("Provided XML file doesn't exist");
                throw new InvalidOperationException("Provided XML file doesn't exist");
            }
            // Use reader to open document, load content and close.
            using (var reader = new XmlTextReader(xmlFileStorage))
            {
                document.Load(reader);
                reader.Close();
            }

            return document;
        }

        #endregion
    }
}