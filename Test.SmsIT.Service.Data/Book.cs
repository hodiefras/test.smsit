﻿using System.Runtime.Serialization;

namespace Test.SmsIT.Service.Data
{
    [DataContract]
    public class Book
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Author { get; set; }

        [DataMember]
        public string Description { get; set; }

        [DataMember]
        public string HyperLink { get; set; }
    }
}