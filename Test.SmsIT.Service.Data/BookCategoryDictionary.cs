﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Test.SmsIT.Service.Data
{
    [DataContract]
    public class BookCategoryDictionary
    {
        [DataMember]
        public Dictionary<string, List<Book>> CategoryBooks { get; set; }
    }
}