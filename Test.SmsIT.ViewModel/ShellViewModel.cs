﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Caliburn.Micro;

namespace Test.SmsIT.ViewModel
{
    public interface IShell
    {
    }

    public class ShellViewModel : PropertyChangedBase, IShell
    {
        private IEventAggregator events;
        private IWindowManager windowsManager;

        public ShellViewModel(IWindowManager windowsManager, IEventAggregator events, BookCategoryCollectionViewModel bookCategoryCollection)
        {
            this.windowsManager = windowsManager;
            events.Subscribe(this);
            this.events = events;
            BookCategoryCollection = bookCategoryCollection;
        }

        public BookCategoryCollectionViewModel BookCategoryCollection { get; private set; }
        
    }

}
