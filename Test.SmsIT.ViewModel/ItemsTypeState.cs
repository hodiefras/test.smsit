﻿namespace Test.SmsIT.ViewModel
{
    /// <summary>
    /// Change viewtype of items in itemscontrol
    /// </summary>
    public enum ItemsTypeState
    {
        Master,
        Detail
    }
}