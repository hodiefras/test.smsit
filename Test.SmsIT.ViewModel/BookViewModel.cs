using Test.SmsIT.Model;

namespace Test.SmsIT.ViewModel
{
    public class BookViewModel
    {
        public string Name { get; private set; }
        public string Author { get; private set; }
        public string Description { get; private set; }
        public string HyperLink { get; private set; }

        public string ImageSource { get; private set; }

        public BookViewModel(string name, string author, string description, string hyperLink, string imageSource)
        {
            Name = name;
            Author = author;
            Description = description;
            HyperLink = hyperLink;
            ImageSource = imageSource;
        }

        public BookViewModel(Book book)
        {
            Name = book.Name;
            Author = book.Author;
            Description = book.Description;
            HyperLink = book.HyperLink;
            ImageSource = book.ImageSource;
        }
    }
}