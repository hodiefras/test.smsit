using System.Collections.ObjectModel;

namespace Test.SmsIT.ViewModel
{
    /// <summary>
    /// Category of books
    /// </summary>
    public class BookCategoryViewModel
    {
        public BookCategoryViewModel(ObservableCollection<BookViewModel> initCollection, string initName)
        {
            BookCollection = initCollection;
            Name = initName;
        }

        public string Name { get; private set; }

        /// <summary>
        /// Count of books
        /// need for UI
        /// </summary>
        public int Count
        {
            get { return BookCollection.Count; }
        }

        /// <summary>
        /// All books
        /// </summary>
        public ObservableCollection<BookViewModel> BookCollection { get; private set; }
    }
}