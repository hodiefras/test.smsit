﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Caliburn.Micro;
using Test.SmsIT.Model;

namespace Test.SmsIT.ViewModel
{
    /// <summary>
    ///     Main viewmodel for represent books
    /// </summary>
    public class BookCategoryCollectionViewModel : PropertyChangedBase
    {
        #region Private fields

        private readonly ObservableCollection<BookCategoryViewModel> categoryCollection =
            new ObservableCollection<BookCategoryViewModel>();

        private BookCategoryCollection bookCategoryCollection;
        private bool isMasterPressed;
        private ItemsTypeState itemsControlViewType = ItemsTypeState.Detail;
        private BookCategoryViewModel selectedCategory;

        #endregion

        #region Constructors

        /// <summary>
        ///     Main constructor
        /// </summary>
        /// <param name="initBookCategoryCollection">object of model</param>
        public BookCategoryCollectionViewModel(BookCategoryCollection initBookCategoryCollection)
        {
            bookCategoryCollection = initBookCategoryCollection;
            ConvertToCategoryCollection(bookCategoryCollection.GetBookDictionary());
        }

        /// <summary>
        ///     Constructor for debug UI
        /// </summary>
        public BookCategoryCollectionViewModel()
        {
            var bookCollection = new ObservableCollection<BookViewModel>
            {
                new BookViewModel("Programming", "Crow A.A.", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png"),
                new BookViewModel("Math", "Raa", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png"),
                new BookViewModel("Alg", "Gaa", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png")
            };
            categoryCollection.Add(new BookCategoryViewModel(bookCollection, "Programming"));
            bookCollection = new ObservableCollection<BookViewModel>
            {
                new BookViewModel("Algorithms", "Crow A.A.", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png"),
                new BookViewModel("Discrete mathematics", "Raa", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png"),
                new BookViewModel("Automata theory", "Gaa D.D.", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png"),
                new BookViewModel("Data Structures", "Gaa", "Something about programming",
                    "mail.google.com/mail/u/0/#inbox", "/Resources/Images/crow.png")
            };
            categoryCollection.Add(new BookCategoryViewModel(bookCollection, "Theory of Computer Science"));
        }

        #endregion

        #region Public properties for View

        public bool IsMasterPressed
        {
            get { return isMasterPressed; }
            set
            {
                isMasterPressed = value;
                NotifyOfPropertyChange();
            }
        }

        public ItemsTypeState ItemsControlViewType
        {
            get { return itemsControlViewType; }
            set
            {
                if (itemsControlViewType == value)
                    return;

                itemsControlViewType = value;
                NotifyOfPropertyChange(() => itemsControlViewType);
            }
        }

        public ObservableCollection<BookCategoryViewModel> CategoryCollection
        {
            get { return categoryCollection; }
        }

        public BookCategoryViewModel SelectedCategory
        {
            get { return selectedCategory ?? CategoryCollection.First(); }
            set
            {
                selectedCategory = value;
                NotifyOfPropertyChange();
            }
        }

        #endregion

        #region Private convert methods

        private void ConvertToCategoryCollection(Dictionary<string, List<Book>> initCategoryDictionary)
        {
            foreach (var el in initCategoryDictionary)
            {
                var bookViewModelCategoryCollection = new ObservableCollection<BookViewModel>();
                foreach (Book book in el.Value)
                {
                    bookViewModelCategoryCollection.Add(new BookViewModel(book.Name, book.Author, book.Description,
                        book.HyperLink, "/Resources/Images/crow.png"));
                }
                var category = new BookCategoryViewModel(bookViewModelCategoryCollection, el.Key);
                categoryCollection.Add(category);
            }
        }

        #endregion

        #region UI actions

        public void SetMaster()
        {
            ItemsControlViewType = ItemsTypeState.Master;
            IsMasterPressed = true;
        }

        public void SetDetail()
        {
            ItemsControlViewType = ItemsTypeState.Detail;
            IsMasterPressed = false;
        }

        public void ChangeCategory(string name)
        {
            SelectedCategory = CategoryCollection.First(t => t.Name.Equals(name));
        }

        #endregion
    }
}