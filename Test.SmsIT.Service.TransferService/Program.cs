﻿using Autofac;
using Test.SmsIT.DAL.Data;
using Test.SmsIT.Service.Contracts;
using Test.SmsIT.Service.ServerModel;
using Topshelf;

namespace Test.SmsIT.Service.TransferService
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            //ConfigureContainer(builder);
            builder.RegisterType<Repository>().As<IRepository>();

            builder.RegisterType<TransferService>().UsingConstructor(typeof(IRepository));
            var container = builder.Build();
            
            
            HostFactory.Run(x =>
            {
                x.Service(() => new WindowsService<TransferService>(container.Resolve<TransferService>));
                x.RunAsLocalSystem();
                x.SetDescription("TransferService");
                x.SetDisplayName("TransferService");
                x.SetServiceName("TransferService");
            });
        }

        private static void ConfigureContainer(ContainerBuilder builder)
        {
            builder.Register(c => new Repository()).As<IRepository>();

            builder.Register(c => new TransferService(c.Resolve<IRepository>()));
        }
    }
}