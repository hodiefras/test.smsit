﻿using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Test.SmsIT.DAL.Data;
using Test.SmsIT.Service.Contracts;
using Test.SmsIT.Service.Data;
using Book = Test.SmsIT.Service.Data.Book;
using DALBook = Test.SmsIT.DAL.Data.Book;

namespace Test.SmsIT.Service.TransferService
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class TransferService : ITransferService
    {
        #region Логгер

        //private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        #endregion

        private readonly IRepository repository;

        public TransferService(IRepository argRepository)
        {
            repository = argRepository;
        }

        public BookCategoryDictionary GetBookCategoryList()
        {
            List<DALBook> books = repository.GetBooksDictionary();
            var bookCategoryDictionary = new BookCategoryDictionary();
            bookCategoryDictionary.CategoryBooks = new Dictionary<string, List<Book>>();
            foreach (var category in books.GroupBy(t => t.Category))
            {
                List<Book> categoryBook =
                    category.Select(
                        book =>
                            new Book
                            {
                                Author = book.Author,
                                Name = book.Name,
                                Description = book.Description,
                                HyperLink = book.Link
                            }).ToList();
                bookCategoryDictionary.CategoryBooks.Add(category.Key, categoryBook);
            }
            return bookCategoryDictionary;
        }
    }
}