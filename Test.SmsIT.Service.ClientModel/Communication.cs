﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Test.SmsIT.Service.ClientModel
{
    /// <summary>
    ///     Proxy class for communicate with service
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Communication<T>
    {
        #region Logger

        //private static readonly ILog Logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Private fields

        /// <summary>
        ///     Конечная точка
        /// </summary>
        private readonly string endpointName;

        /// <summary>
        ///     Переопределенной поведение для конечной точки
        /// </summary>
        private IEndpointBehavior endpointBehaviour;

        #endregion

        #region Public Methods

        /// <summary>
        ///     Constructor
        /// </summary>
        /// <param name="endpointName">initialize endpoint</param>
        public Communication(string endpointName)
        {
            this.endpointName = endpointName;
        }

        /// <summary>
        ///     Usefull in future
        /// </summary>
        /// <param name="behaviour"></param>
        public void RegisterBehaviour(IEndpointBehavior behaviour)
        {
            //Logger.Debug("Trying to register behavior");
            endpointBehaviour = behaviour;
        }

        /// <summary>
        ///     Something like Proxy-pattern.
        /// </summary>
        /// <param name="action"></param>
        public void Using(Action<T> action)
        {
            // We always creating new. TODO make 1 for service. 
            //Logger.Debug("Creating fabric with previously specified endpoint");
            var channelFactory = new ChannelFactory<T>(endpointName);
            if (endpointBehaviour != null)
            {
                channelFactory.Endpoint.Behaviors.Add(endpointBehaviour);
            }
            // Create channel
            T channel = channelFactory.CreateChannel();
            try
            {
                action(channel);
                ((IClientChannel) channel).Close();
            }
            catch (Exception exception)
            {
                //Logger.Error(m => m("Error with channel: {0}", exception));
                var clientInstance = ((IClientChannel) channel);
                if (clientInstance.State == CommunicationState.Faulted)
                {
                    clientInstance.Abort();
                    channelFactory.Abort();
                }
                    // closing instance thats didnot close
                else if (clientInstance.State != CommunicationState.Closed)
                {
                    clientInstance.Close();
                    channelFactory.Close();
                }
                throw;
            }
        }

        #endregion
    }
}