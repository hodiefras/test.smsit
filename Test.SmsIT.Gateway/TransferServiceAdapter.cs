﻿using System.Collections.Generic;
using System.Linq;
using Test.SmsIT.Model;
using Test.SmsIT.Service.ClientModel;
using Test.SmsIT.Service.Contracts;
using Test.SmsIT.Service.Data;
using BookService = Test.SmsIT.Service.Data.Book;
using BookModel = Test.SmsIT.Model.Book;

namespace Test.SmsIT.Gateway
{
    /// <summary>
    ///     Class-adapter for service
    ///     client of service
    /// </summary>
    public class TransferServiceAdapter : ITransferServiceAdapter
    {
        private readonly Communication<ITransferService> communication;

        public TransferServiceAdapter(Communication<ITransferService> initCommunication)
        {
            communication = initCommunication;
        }

        /// <summary>
        ///     Get service
        /// </summary>
        /// <returns>collection for creating main view model</returns>
        public Dictionary<string, List<BookModel>> GetTransferBook(
            )
        {
            var books = new BookCategoryDictionary();
            communication.Using(service => { books = service.GetBookCategoryList(); });
            if (books != null)
            {
                return ConvertDtoToModel(books);
            }
            //Logger.Error("Service dont send data")
            return null;
        }

        /// <summary>
        ///     Method for convert dto objects
        /// </summary>
        /// <param name="bookCategoryDictionary">dto-dictionary</param>
        /// <returns></returns>
        private Dictionary<string, List<BookModel>> ConvertDtoToModel(BookCategoryDictionary bookCategoryDictionary)
        {
            var result = new Dictionary<string, List<BookModel>>();
            foreach (var el in bookCategoryDictionary.CategoryBooks)
            {
                List<BookModel> bookCollection =
                    el.Value.Select(book => new BookModel(book.Name, book.Author, book.Description, book.HyperLink))
                        .ToList();

                result.Add(el.Key, bookCollection);
            }
            return result;
        }
    }
}