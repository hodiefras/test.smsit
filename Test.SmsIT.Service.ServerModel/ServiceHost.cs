﻿using System.ServiceModel;

namespace Test.SmsIT.Service.ServerModel
{
    /// <summary>
    ///     May be usefull in future
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ServiceHost<T> : ServiceHost
    {
        public ServiceHost(T instance)
            : base(instance)
        {
        }
    }
}