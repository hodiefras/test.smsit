﻿using System;
using Topshelf;

namespace Test.SmsIT.Service.ServerModel
{
    /// <summary>
    ///     Inheritance topshelf class for hosting services
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class WindowsService<T> : ServiceControl
    {
        private readonly Func<T> instanceCreator;
        private ServiceHost<T> serviceHost;

        public WindowsService(Func<T> instanceCreator)
        {
            this.instanceCreator = instanceCreator;
        }

        public bool Start(HostControl hostControl)
        {
            serviceHost = new ServiceHost<T>(instanceCreator());
            serviceHost.Open();

            return true;
        }

        public bool Stop(HostControl hostControl)
        {
            serviceHost.Close();

            return true;
        }
    }
}